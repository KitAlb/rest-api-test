package com.epam.lab.library.wire.author;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

@Value
@Builder
@JsonDeserialize(builder = Name.NameBuilder.class)
public class Name implements Serializable {

    private String first;
    private String second;

    @JsonPOJOBuilder(withPrefix = StringUtils.EMPTY)
    public static class NameBuilder {}

}

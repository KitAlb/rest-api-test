package com.epam.lab.library.wire.author;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Value;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

@Value
@Builder
@JsonDeserialize(builder = Author.AuthorBuilder.class)
public class Author implements Serializable {

    private Long authorId;
    private Name authorName;
    private String nationality;
    private Birth birth;
    private String authorDescription;

    @JsonPOJOBuilder(withPrefix = StringUtils.EMPTY)
    public static class AuthorBuilder {}

}

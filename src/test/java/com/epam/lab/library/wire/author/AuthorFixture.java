package com.epam.lab.library.wire.author;

import java.time.LocalDate;

class AuthorFixture {

    static final String ROOT_PATH = "api/library/";

    static final Long NEW_AUTHOR_ID = 13L;
    static final Long AUTHOR_ID_INVALID = 9999L;
    static final Author NEW_AUTHOR = Author.builder()
            .authorId(NEW_AUTHOR_ID)
            .authorName(Name.builder()
                    .first("Deja")
                    .second("Klocko")
                    .build())
            .nationality("Cambodian")
            .birth(Birth.builder()
                    .city("Port Birdie")
                    .country("Norfolk Island")
                    .date(LocalDate.of(1965, 11, 16))
                    .build())
            .authorDescription("Description")
            .build();

}

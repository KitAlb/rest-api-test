package com.epam.lab.library.wire.author;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.*;

import static com.epam.lab.library.wire.author.AuthorFixture.*;
import static io.restassured.RestAssured.when;
import static io.restassured.RestAssured.with;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AuthorTest {

    @BeforeAll
    static void setup() {
        RestAssured.port = 8081;
    }

    @Test
    @DisplayName("GIVEN valid author " +
            "WHEN POST author " +
            "THEN return valid response")
    @Order(1)
    public void addAuthorPositive() {
        // GIVEN

        // WHEN
        with()
                .contentType(ContentType.JSON)
                .body(NEW_AUTHOR)
                .when()
                .post(ROOT_PATH + "author")
                .then()

                // THEN
                .assertThat()
                .statusCode(HttpStatus.SC_CREATED);
    }

    @Test
    @DisplayName("GIVEN existing author " +
            "WHEN POST author " +
            "THEN return valid response")
    @Order(2)
    public void addExistingAuthorNegative() {
        // GIVEN

        // WHEN
        with()
                .contentType(ContentType.JSON)
                .body(NEW_AUTHOR)
                .when()
                .post(ROOT_PATH + "author")
                .then()

                // THEN
                .assertThat()
                .statusCode(HttpStatus.SC_CONFLICT);
    }

    @Test
    @DisplayName("GIVEN valid request " +
            "WHEN GET authors " +
            "THEN return valid response")
    @Order(3)
    public void getAllPositive() {
        // GIVEN

        // WHEN
        Author[] response = when()
                .get(ROOT_PATH + "authors")
                .then()

                // THEN
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .extract().as(Author[].class);

        // your service has a bug when saving Author. Expecting this test to fail
        assertThat(response)
                .contains(NEW_AUTHOR);
    }

    @Test
    @DisplayName("GIVEN valid authorId " +
            "WHEN GET author " +
            "THEN return valid response")
    @Order(4)
    public void getByIdPositive() {
        // GIVEN

        // WHEN
        Author response = when()
                .get(ROOT_PATH + "author/" + NEW_AUTHOR_ID.toString())
                .then()

                // THEN
                .assertThat()
                .statusCode(HttpStatus.SC_OK)
                .extract().as(Author.class);

        // your service has a bug when saving Author. Expecting this test to fail
        assertThat(response)
                .isEqualTo(NEW_AUTHOR);
    }

    @Test
    @DisplayName("GIVEN invalid authorId " +
            "WHEN GET author " +
            "THEN return valid response")
    @Order(5)
    public void getByIdNegative() {
        // GIVEN

        // WHEN
        when()
                .get(ROOT_PATH + "author/" + AUTHOR_ID_INVALID.toString())
                .then()

                // THEN
                .assertThat()
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }

}
